import {ApiRequestService} from '../lib/index';
import * as chai from 'chai';

const expect = chai.expect;
describe('My math library', () => {

    it('should be able to add things correctly', () => {
        expect(ApiRequestService.add(3, 4)).to.equal(7);
    });


    it('should be able to substract things correctly', () => {
        expect(ApiRequestService.substract(3, 4)).to.equal(-1);
    });

});
