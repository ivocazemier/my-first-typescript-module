class ApiRequestService {

    public static add(a: number, b: number): number {
        return a + b;
    }

    public static substract(a: number, b: number): number {
        return a - b;
    }
}

export {ApiRequestService};
